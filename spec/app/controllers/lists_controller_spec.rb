# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ListsController, type: :controller do
  describe '#show' do
    let(:board_id) {"ABC123"}
    let(:api_provider_response) { file_fixture('trello_get_response.json').read }

    before(:each) do
      stub_request(:get, "https://api.com/1/boards/ABC123/lists?card_fields=name&cards=open").
        to_return(status: 200, body: api_provider_response, headers: {})
    end

    it 'returns lists' do
      get :show

      expect(response).to have_http_status(:success)
    end
  end

  describe '#create_card' do
    let(:post_response) { file_fixture('trello_post_response.json').read }

    it 'creates the card' do
      stub_request(:post, "https://api.com/1/cards?idList=id&name=hello").
        to_return(status: 200, body: post_response, headers: {})
      post :create_card, params: { list: { list_id: 'id', card_details: 'hello' } }

      expect(response).to have_http_status(:success)
    end

    it 'returns unprocessable_entity if params are empty' do
      post :create_card, params: { list: { list_id: '', card_details: 'hello' } }

      expect(response).to have_http_status(:unprocessable_entity)
    end
  end
end
