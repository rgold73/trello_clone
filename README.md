# README

Simple TrelloClone

## Ruby version
ruby-2.7.2

## Rails and React
- Rails api code can be found in the conventional space
- React can be found in app/javascript/packs

## Configuration
* Set the following environment variables
    - TRELLO_URL='https://api.trello.com'
    - TRELLO_CONSUMER_KEY='2f41cce123456789107fea7862720'
    - TRELLO_TOKEN='12345678dae12d85a2c702fa871918b1cc26c70d46becf1234578'
    - BOARD_ID='AbClbWbG'

- Board ID can be obtained from the url in trello.
- Consumer key and the token can be obtained from https://trello.com/app-key/

## Start the service
* export the configs mentioned above
* rails s
* Browse to http://localhost:3000/

## How to run the test suite
bundle exec rspec spec

## api external requests:
* GET https://api.trello.com/1/boards/#{@board_id}/lists?cards=all&card_fields=name
returns the lists of a board filtered down to the associated cards and ids 

* POST https://api.trello.com/1/cards { idList, name }
Returns a card object on success

## CI/CD
.gitlab-ci.yml
https://gitlab.com/matthewbried/trello_clone/-/pipelines

## Screenshots
![Task A](img/offerzen.png "main page")

![Task B](img/offerzen2.png "Modal adding card")

## Task 
Task C
I did end up going over the time frame although I'm sure some of this was the normal fighting with my local envirmonment issues and not the coding itself.

I spent some time on figuring out the oauth since it's more secure, instead of passing in the secret via normal GET requests.

I am placing a trello api provider in the lib folder. This makes it easier to switch out with a new provider while keeping the contract more or less the same in rest of the project. And if there were more applications this provider can be easily shared.

I am using environment variables for secret management as the quickest path to victory, ideally this should be handled by better secret management and a config.yml

As the task has few requirements and a 4 hour deadline I've opted for a MVP approach and have excluded a DB meaning I lose out on the active record goodness but the project will be very small and simple. Should more requirements come in I will follow the Agile approach of building it in when I need it, the specs should remain more or less the same and the front end shouldn't be affected.

I spent some time with yarn and configuring webpacker due to environment issues.

I decided to use material-ui with webpacker and React. I struggled a bit with the styling as I'm used to just importing a kit in my previous roles.

I think I spent most of my time figuring out the interactions between the rails app and the react frontend. I had to monkey patch a CSRF issue and due to time constraints I don't have as robust in depth testing as I would have liked especially on the controller. 

I think if I had to do this again I'd opted for using functions and hooks in the react over the class component and if I had more time I would refactor the react app into smaller components as well as learning and implementing jest or some other testing frontend tool. I would also redo the way I access the first list and second list instead of hard coding the access so that it's more extensible to adding more lists etc.

If I had to do it differently for the backend I think I would setup a DB and have a board that has many lists and lists that has many cards. As is though I think I met the brief. 

There are a few minor bug issues I didn't get to resolving like closing the modal once the promise succeeds and adds the card. As well as stopping the update if the onChange event detects invalid characters.

I would also really have liked to add more validations. I'd also have liked to have dockerized this and hosted it on heroku.


Overall it was pretty fun. Thank you!
