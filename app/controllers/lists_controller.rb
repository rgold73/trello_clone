# frozen_string_literal: true

class ListsController < ApplicationController
  # TODO: get react to pass the right headers that this skip isnt needed CSRF issue
  skip_before_action :verify_authenticity_token

  def show
    @lists = Board.lists
    render('show')
  end

  def create_card
    list_id = params.dig(:list, :list_id)
    card_details = params.dig(:list, :card_details)
    return render json: { message: 'paramaters are missing' }, status: :unprocessable_entity if list_id.blank? || card_details.blank?

    result = Board.create_card(list_id: list_id, card_details: card_details)
    render json: { data: result }
  end
end
