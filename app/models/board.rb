# frozen_string_literal: true

class Board
  def self.lists
    lists = {}
    TrelloProviderApi.new.list_details.each do |list|
      lists[list[:id]] = { list_name: list[:name], cards: list[:cards].map { |hash| hash[:name] } }
    end
    lists
  end

  def self.create_card(list_id:, card_details:)
    # TODO: more validation
    return false if list_id.blank? || card_details.blank?

    TrelloProviderApi.new.create_card(list_id, card_details)
  end
end
